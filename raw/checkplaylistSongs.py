import requests
from bs4 import BeautifulSoup
from datetime import datetime
import os
import json
import sys
import shutil

os.chdir("/Users/yfm/Desktop/spider/kugou")
filepath = sys.path[0] + f'/周杰伦.json'

sourceDirectory = "/Users/yfm/Desktop/spider/kugou/music"
destinationDirectory = "/Users/yfm/Desktop/spider/kugou/playlist"

existList = []
with open(filepath, 'r', encoding='utf-8') as file:
    content = file.read()
    data = json.loads(content)
    ids = []
    for item in data:
        songname = item['song_name']
        id = item['encode_album_audio_id']
        ids.append(id)
        sourcefilepath = sourceDirectory + f'/{songname}.mp3'
        if os.path.exists(sourcefilepath):
            existList.append(item)
            print(songname + 'exist')

        if os.path.isfile(sourcefilepath):
            shutil.copy2(sourcefilepath, destinationDirectory)
    # print(ids)
