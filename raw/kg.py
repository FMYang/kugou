import requests, json, sys, os, threading
import queue, concurrent.futures, re, time, hashlib
from urllib.request import quote
from bs4 import BeautifulSoup as bs

# 榜单信息
# rankList = [
#         {"title": "酷狗飙升榜", "id": 6666},
#         {"title": "top500", "id": 8888},
#         {"title": "80后热歌榜", "id": 49225},
#         {"title": "90后热歌榜", "id": 49223},
#         {"title": "00后热歌榜", "id": 49224},
#         {"title": "蜂鸟流行音乐榜", "id": 59703},
#         {"title": "抖音热歌榜", "id": 52144},
#         {"title": "快手热歌榜", "id": 52767},
#         {"title": "DJ热歌榜", "id": 24971},
#         {"title": "内地榜", "id": 31308},
#         {"title": "香港地区榜", "id": 31313},
#         {"title": "台湾地区榜", "id": 54848},
#         {"title": "欧美榜", "id": 31310},
#         {"title": "综艺新歌榜", "id": 46910},
#         {"title": "说唱先锋榜", "id": 44412},
#         {"title": "影视金曲榜", "id": 33163},
#         {"title": "粤语金曲榜", "id": 33165},
#         {"title": "ACG新歌榜", "id": 33162},
#         {"title": "酷狗音乐人原创榜", "id": 30972},
#         {"title": "酷狗雷达榜", "id": 37361},
#         {"title": "joox本地热歌榜", "id": 42807},
#         {"title": "KKBOX风云榜", "id": 42808},
#         {"title": "电音热歌榜", "id": 33160},
#         {"title": "韩国榜", "id": 31311},
#         {"title": "日本榜", "id": 31312},
#         {"title": "欧美金曲榜", "id": 33166},
#         {"title": "美国BillBoard榜", "id": 4681},
#         {"title": "英国单曲榜", "id": 4680},
#         {"title": "日本公信榜", "id": 4673},
#         {"title": "韩国Melon音乐榜", "id": 38623},
#         {"title": "日本SPACE SHOWER榜", "id": 46868},
#         {"title": "Beatport电子舞曲榜", "id": 25028},
#         {"title": "小语种热歌榜", "id": 36107}
#         ]

class KG:
    def __init__(self):
        self.currentDir = sys.path[0]
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0',
            # 'Cookie': 'kg_mid=c3c3374152df945246969fee3f15f108; kg_dfid=1Jcfej3YdDZy2atfXD11pFRw; Hm_lvt_aedee6983d4cfc62f509129360d6bb3d=1700105234; KuGooRandom=66551700105260332; YD00975034853784%3AWM_TID=j5AeyEcJp01BEQAVBVfBmu%2F7F9OgxDbh; CheckCode=czozMjoiYWRhYzg0YmVlNmZjZTczOTZkNTkxZWExN2I3ZThhNjAiOw%3D%3D;YD00975034853784%3AWM_NI=P%2BzHIRF59hfcV7V%2FOPexU995hqXuAECi3JtJJ5JGOm9rUOciN5My0XOzIbpVsCUVADo%2BtVPMEbEvSe2c1J92nBq8WsHWfGzRBPN%2B1Pq8cibB%2BmH%2BzHUlqtg3ypZawkddOUo%3D;YD00975034853784%3AWM_NIKE=9ca17ae2e6ffcda170e2e6eeafb354fcbdbebab17da8ef8aa2d14a939e8eadd17996e89897d67fe99fadaeb12af0fea7c3b92a94f5b6a9cc5a86ebf789e56996e99ad0ce5ca7a89bdabc3facaf9896c44f81b7fdabf7428aeb86a8ce67fbae9bb0e2638eeb839ab3548da68783f35eb19d9bb7c450adf5bcb7d34787be8b97d26ff2bdffabbc3aae9ebfd9d6798c89aaa8f95a829082ccb45cb3e8ad8ae14ae9b388ccf94497b4bf95e639bbb09682f661a8bc81b6ee37e2a3;gdxidpyhxdE=wUIS7%5C1XBG3tuSzUVtTWHR0gmjSYvatOvtv3JOayPp1mvqgTsjvMVP3ZvTo5O%2F98Rt119ng7X9r8IpATgd45itboWaTos1A1H1%2BErg%2FaX71CM%5ChvXibDrqu01OoUWn2Trf9M%5CEtm72MuQe7Uv277gR1bC32pHkYW8ri2pS4%2FIoLk6wAy%3A1701658433080;cct=f0739ac8;ACK_SERVER_10015=%7B%22list%22%3A%5B%5B%22bjlogin-user.kugou.com%22%5D%5D%7D;kg_dfid_collect=d41d8cd98f00b204e9800998ecf8427e;Hm_lpvt_aedee6983d4cfc62f509129360d6bb3d=1701759011;'
        }
        self.cookies = {
            'kg_mid': 'c3c3374152df945246969fee3f15f108',
            'kg_dfid': '1Jcfej3YdDZy2atfXD11pFRw',
            'Hm_lvt_aedee6983d4cfc62f509129360d6bb3d': '1700105234',
            'KuGooRandom': '66551700105260332',
            'YD00975034853784%3AWM_TID': 'j5AeyEcJp01BEQAVBVfBmu%2F7F9OgxDbh',
            'CheckCode': 'czozMjoiYWRhYzg0YmVlNmZjZTczOTZkNTkxZWExN2I3ZThhNjAiOw%3D%3D',
            'YD00975034853784%3AWM_NI': 'P%2BzHIRF59hfcV7V%2FOPexU995hqXuAECi3JtJJ5JGOm9rUOciN5My0XOzIbpVsCUVADo%2BtVPMEbEvSe2c1J92nBq8WsHWfGzRBPN%2B1Pq8cibB%2BmH%2BzHUlqtg3ypZawkddOUo%3D',
            'YD00975034853784%3AWM_NIKE': '9ca17ae2e6ffcda170e2e6eeafb354fcbdbebab17da8ef8aa2d14a939e8eadd17996e89897d67fe99fadaeb12af0fea7c3b92a94f5b6a9cc5a86ebf789e56996e99ad0ce5ca7a89bdabc3facaf9896c44f81b7fdabf7428aeb86a8ce67fbae9bb0e2638eeb839ab3548da68783f35eb19d9bb7c450adf5bcb7d34787be8b97d26ff2bdffabbc3aae9ebfd9d6798c89aaa8f95a829082ccb45cb3e8ad8ae14ae9b388ccf94497b4bf95e639bbb09682f661a8bc81b6ee37e2a3',
            'gdxidpyhxdE': 'wUIS7%5C1XBG3tuSzUVtTWHR0gmjSYvatOvtv3JOayPp1mvqgTsjvMVP3ZvTo5O%2F98Rt119ng7X9r8IpATgd45itboWaTos1A1H1%2BErg%2FaX71CM%5ChvXibDrqu01OoUWn2Trf9M%5CEtm72MuQe7Uv277gR1bC32pHkYW8ri2pS4%2FIoLk6wAy%3A1701658433080',
            'cct': 'f0739ac8',
            'ACK_SERVER_10015': '%7B%22list%22%3A%5B%5B%22bjlogin-user.kugou.com%22%5D%5D%7D',
            'kg_dfid_collect': 'd41d8cd98f00b204e9800998ecf8427e',
            'Hm_lpvt_aedee6983d4cfc62f509129360d6bb3d': '1701759011'
        }

        directory = self.currentDir + "/music"
        if not os.path.exists(directory):
            os.makedirs(directory)
            print(f"目录{directory}创建成功")

    # 获取排行榜歌曲
    # 参数：榜单id
    def getRankSongs(self, randid):
        songs = []
        api = f"http://mobilecdnbj.kugou.com/api/v3/rank/song?rankid={randid}&page=1&pagesize=500"
        resp = json.loads(requests.get(api).text)
        info = resp['data']['info']
        
        for item in info:
            filename = item['filename']
            id = item['album_audio_id']
            hash = item['hash']
            album_id = item['album_id']
            jsonData = {"album_audio_id": id, "filename": filename, "hash": hash, "album_id": album_id}
            print(filename)
            songs.append(jsonData)
        return songs
    
    # 获取歌手的所有专辑
    # 参数：歌手id
    def getSingerAlbums(self, singerId):
        alumbs = []
        api = f'http://mobilecdnbj.kugou.com/api/v3/singer/album?singerid={singerId}&page=1&pagesize=500'
        resp = json.loads(requests.get(api).text)
        info = resp['data']['info']
        
        for item in info:
            albumname = item['albumname']
            albumid = item['albumid']
            alumbs.append({"albumname": albumname, "albumid": albumid})

        return alumbs

    # 获取专辑的所有歌曲
    # 参数：专辑id
    def getAlbumSongs(self, albumId):
        songNames = []
        api = f'http://mobilecdn.kugou.com/api/v3/album/song?albumid={albumId}&page=1&pagesize=500'
        resp = json.loads(requests.get(api).text)
        info = resp['data']['info']
        
        for item in info:
            filename = item['filename']
            id = item['album_audio_id']
            jsonData = {"album_audio_id": id, "filename": filename}
            songNames.append(jsonData)
        
        return songNames
    
    # 获取歌手所有歌曲，sorttype：0按热度排序，1按时间排序
    # 参数：歌手id
    def getSingerSongs(self, singerid, pagesize=500):
        songNames = []
        api = f'http://mobilecdnbj.kugou.com/api/v3/singer/song?singerid={singerid}&page=1&pagesize={pagesize}&sorttype=0'
        resp = json.loads(requests.get(api).text)
        info = resp['data']['info']
        
        for item in info:
            filename = item['filename']
            id = item['album_audio_id']
            hash = item['hash']
            album_id = item['album_id']
            jsonData = {"album_audio_id": id, "filename": filename, "hash": hash, "album_id": album_id}
            print(filename)
            songNames.append(jsonData)
        return songNames
    
    # 获取歌手信息
    # 参数：歌手id
    def getsinderInfo(self, singerid):
        try:
            api = f'http://mobilecdngz.kugou.com/api/v3/singer/info?singerid={singerid}'
            response = requests.get(api)
            response.raise_for_status()
            resp = json.loads(response.text)
            info = resp['data']
            peripherycount = info['peripherycount']
            songcount = info['songcount']
            singername = info['singername']
            profile = info['profile']
            style = info['style']
            mvcount = info['mvcount']
            singerid = info['singerid']
            grade = info['grade']
            albumcount = info['albumcount']
            intro = info['intro']
            classical_work_total = info['classical_work_total']
            has_long_intro = info['has_long_intro']
            charge_video_count = info['charge_video_count']
            identity = info['identity']
            alias = info['alias']
            imgurl = info['imgurl']
            is_settled_author = info['is_settled_author']
            jsonData = {
                "peripherycount": peripherycount,
                "songcount": songcount,
                "singername": singername,
                "profile": profile,
                "style": style,
                "mvcount": mvcount,
                "singerid": singerid,
                "grade": grade,
                "albumcount": albumcount,
                "intro": intro,
                "classical_work_total": classical_work_total,
                "has_long_intro": has_long_intro,
                "charge_video_count": charge_video_count,
                "identity": identity,
                "alias": alias,
                "is_settled_author": is_settled_author,
                "imgurl": imgurl
                }
            print(singerid, singername)
            return jsonData
        except:
            print(singerid, 'except')
            return None
        time.sleep(0.5)

    # 获取encode_album_audio_id
    # 参数：歌曲hash和所属专辑id
    # def getSong_encode_album_audio_id(self, hash, album_id):
    #     data_regex = re.compile(r'jQuery.*?[({].*?"data":(.*?)}[)]', re.S)
    #     api = f"https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback=jQuery191053318263960215_1626866592344&hash={hash}&dfid=1tXkst0i97Rq4RW0pz15GjrP&mid=3196606d7d3ff0207a473da58e0b44b3&platid=4&album_id={album_id}&_=1626866592346"
    #     try:
    #         response = requests.get(api, headers=self.headers)
    #         response.raise_for_status() # 校验状态码
    #         resp = response.text
    #         data_text = data_regex.findall(resp)[0]
    #         data = json.loads(data_text)

    #         encode_album_audio_id = data.get('encode_album_audio_id', "")
    #         song_name = data.get('song_name', "")
            
    #         print(song_name, ' ', encode_album_audio_id)
    #         return encode_album_audio_id
    #     except:
    #         print('except')
    #         pass

    # 获取encode_album_audio_id
    # 参数：歌曲hash
    def getSong_encode_album_audio_id(self, hash, id):
        # 通过hash获取encode_album_audio_id
        dataUrl = f'https://www.kugou.com/yy/index.php?r=play/getdata&hash={hash}&album_audio_id={id}'
        response1 = requests.get(dataUrl, headers=self.headers, cookies=self.cookies)
        # response1 = requests.get(dataUrl, headers=self.headers)
        resp1 = json.loads(response1.text)
        data = resp1['data']
        encode_album_audio_id = data['encode_album_audio_id']
        print(data['play_url'])
        return encode_album_audio_id
    
    # 搜索歌名，获取encode_album_audio_id
    # 参数：歌名
    def searchSong(self, text):
        url='https://songsearch.kugou.com/song_search_v2?callback=jQuery1124018638456262994435_%d&keyword=%s&page=1&pagesize=30&userid=-1&clientver=&platform=WebFilter&tag=em&filter=2&iscorrection=1&privilege_filter=0&_=%d'%(
        int(time.time()*1000)-2,quote(text),int(time.time()*1000))

        headers={
            'Referer':'https://www.kugou.com/yy/html/search.html',
            'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36'
        }
        dfid='1aAcF31Utj2l0ZzFPO0Yjss0'
        mid='7df6a46dd0a23d920fe7929f492ef251'
        try:
            res = requests.get(url, headers)
            res.raise_for_status()
            response=json.loads(re.findall('[(](.*)[)]',requests.get(url=url,headers=headers).content.decode('utf-8'))[0])['data']['lists']
            Hashs,informations = [x['FileHash'] for x in response],list(map(lambda x:[x['SongName'].replace('<em>','').replace('</em>',''),x['SingerName']],response))[0]
            hash = Hashs[0]
            url='https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback=jQuery19107306344625120932_%d&hash=%s&album_id=&dfid=%s&mid=%s&platid=4&_=%d'%(
                int(time.time()*1000)-1,hash,dfid,mid,int(time.time()*1000))
            headers={
                'Referer':'https://www.kugou.com/song/',
                'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; rv:11.0) like Gecko'
            }
            data = json.loads(re.findall('[(](.*)[)]', requests.get(url=url, headers=headers).content.decode('utf-8'))[0])
            if response!='':
                id = data['data']['encode_album_audio_id']
                print(informations, id)
                return id
            else:
                print("error")
        except:
            pass

    # 搜索歌名，获取encode_album_audio_id
    def search(self, text):
        searchUrl = f'https://songsearch.kugou.com/song_search_v2?keyword={quote(text)}&page=1&pagesize=30&filter=2&platform=WebFilter'
        try:
            response = requests.get(searchUrl, headers=self.headers)
            resp = json.loads(response.text)
            info = resp['data']['lists'][0]
            # 获取hash
            hash = info['FileHash']
            mixSongID = info['MixSongID']

            # 通过hash获取encode_album_audio_id
            dataUrl = f'https://www.kugou.com/yy/index.php?r=play/getdata&hash={hash}&album_audio_id={mixSongID}'
            response1 = requests.get(dataUrl, headers=self.headers, cookies=self.cookies)
            resp1 = json.loads(response1.text)
            data = resp1['data']
            encode_album_audio_id = data['encode_album_audio_id']
            print(encode_album_audio_id)
            return encode_album_audio_id
        except:
            print('except')
            pass

    # 获取歌曲下载地址和详细信息
    # 参数：encode_album_audio_id
    def getSonginfo(self, encode_album_audio_id, token=''):
        try:
            # vip歌曲地址（需要vip信息签名，token和userId）
            template = f"https://wwwapi.kugou.com/play/songinfo?srcappid=2919&clientver=20000&clienttime=%s&mid=%s&uuid=%s&dfid=%s&appid=1014&platid=4&encode_album_audio_id=%s&token={token}&userid=1562760404&signature=%s"
            ts = str(int(time.time() * 1000))
            mid = "c3c3374152df945246969fee3f15f108"
            uuid = mid
            dfid = '1Jcfej3YdDZy2atfXD11pFRw'
            sign_template = f"NVPh5oo715z5DIWAeQlhMDsWXXQV4hwtappid=1014clienttime=%sclientver=20000dfid=%sencode_album_audio_id=%smid=%splatid=4srcappid=2919token={token}userid=1562760404uuid=%sNVPh5oo715z5DIWAeQlhMDsWXXQV4hwt"
            sign_str = sign_template % (ts, dfid, encode_album_audio_id, mid, uuid)
            signature = hashlib.md5(sign_str.encode()).hexdigest()
            songurl = template % (ts, mid, uuid, dfid, encode_album_audio_id, signature)
            response = requests.get(songurl)
            response.raise_for_status() # 校验状态码
            resp = response.text
            data = json.loads(resp)['data']

            self.song_name = data.get('song_name', "")
            self.lrc = data.get('lyrics', "")
            self.play_url = data.get('play_url')
            self.play_backup_url = data.get('play_backup_url', "")
            self.timelength = data.get('timelength')
            self.filesize = data.get('filesize')
            self.author_name = data.get('author_name', "")
            self.timelength = data.get('timelength')
            self.is_free_part = data.get('is_free_part')
            self.img = data.get('img', "")
            self.encode_album_id = data.get('encode_album_id', "")
            self.encode_album_audio_id = data.get('encode_album_audio_id', "")
            self.album_audio_id = data.get('album_audio_id', "")
            self.album_id = data.get('album_id', "")
            self.author_id = data.get('author_id', "")
            self.video_id = data.get('video_id', "")
            print(f"{self.song_name} is_free_part={self.is_free_part}")
            if self.is_free_part is None:
                print(f"{self.encode_album_audio_id}失败 去校验验证码，然后继续")
                time.sleep(7)
                return None

            data = {
                "song_name": self.song_name,
                "play_url": self.play_url,
                "play_backup_url": self.play_backup_url,
                "timelength": self.timelength,
                "filesize": self.filesize,
                "author_name": self.author_name,
                "timelength": self.timelength,
                "is_free_part": self.is_free_part,
                "img": self.img,
                "encode_album_id": self.encode_album_id,
                "encode_album_audio_id": self.encode_album_audio_id,
                "album_audio_id": self.album_audio_id,
                "album_id": self.album_id,
                "author_id": self.author_id,
                "video_id": self.video_id,
                "lrc": self.lrc
            }
            return data
        except:
            print("except")
            pass

    # 下载歌曲
    # 参数，需要下载的json文件
    def download(self, filepath):
        musicDir = sys.path[0] + "/music"
        with open(filepath, 'r') as file:
            data = file.read()
            songs = json.loads(data)
            for song in songs:
                song_name = song['song_name']
                songFile = musicDir + f"/{song_name}.mp3"
                try:
                    if not os.path.exists(songFile):
                        play_url = song['play_url']
                        print(f'Downloading {song_name}')
                        try:
                            content = requests.get(play_url, headers=self.headers).content
                        except:
                            content = bytes()
                        try:
                            savepath = os.path.join(musicDir, f'{song_name}.mp3')
                            with open(savepath, 'wb') as f:
                                f.write(content)
                        except:
                            pass
                    else:
                        print('文件已经存在')
                        pass
                except:
                    pass

    # 获取歌单的encode_album_audio_id集合
    # 参数歌单url
    def getPlayListIds(self, url):
        ids = []
        try:
            response = requests.get(url, headers=self.headers)
            response.raise_for_status() # 校验状态码
            song_soup = bs(response.text, 'html.parser')
            song_link_regex = re.compile('https://www.kugou.com/mixsong/[0-9A-Za-z]*\.html')
            encode_album_audio_id_pattern = r'/([^/]+)\.html'
            links = song_soup.find_all('a', {'href': song_link_regex})
            listName = song_soup.find_all('div', class_='mbx')[0].find('span').text
            for link in links:
                link = link['href']
                encode_album_audio_id = re.search(encode_album_audio_id_pattern, link).group(1)
                ids.append(encode_album_audio_id)
            return ids
        except:
            pass

    # 下载单曲
    # 参数：歌曲encode_album_audio_id
    # 参数：用户token，vip歌曲下载需要vip账号的token，免费的歌曲不需要token
    def downloadOnly(self, id, token):
        songInfo = self.getSonginfo(id, token)
        song_name = songInfo['song_name']
        filepath = sys.path[0] + f"/playlist/{song_name}.mp3"
        try:
            if not os.path.exists(filepath):
                play_url = songInfo['play_url']
                print(f'Downloading {song_name}')
                try:
                    content = requests.get(play_url, headers=self.headers).content
                except:
                    content = bytes()
                try:
                    with open(filepath, 'wb') as f:
                        f.write(content)
                except:
                    pass
            else:
                print('文件已经存在')
                pass
        except:
            pass
        
    # 写文件
    def write(self, filepath, jsons):
        with open(filepath, 'w', encoding='utf-8') as file:
            json.dump(jsons, file, ensure_ascii=False, indent=4)
    
    # 多线程处理
    def process_request(self, reqeust_number):
        singerInfo = self.getsinderInfo(reqeust_number)
        if not singerInfo is None:
            return singerInfo
    
if __name__ == '__main__':
    kg = KG()

    # 1.获取歌手信息
    # singerInfo = kg.getsinderInfo(2)
    # print(singerInfo)

    # 2.获取歌手所有歌曲
    # allsongs = kg.getSingerSongs(2)
    # print(allsongs)

    # 3.获取歌手所有专辑
    # allAlbums = kg.getSingerAlbums(3520)
    # print(allAlbums)

    # 4.获取全站所有歌手信息
    # current_dir = sys.path[0]
    # filename = f"singerList.json"
    # file_path = os.path.join(current_dir, filename)

    # with open(file_path, "r", encoding="utf-8") as file:
    #     content = file.read()
    #     contentData = json.loads(content)
    #     id_array = [item["singerid"] for item in contentData]
    #     # id_array = id_array[:100]
    #     # print(id_array)

    #     # 创建线程并启动
    #     threads = []
    #     max_threads = 100
    #     results = []
    #     with concurrent.futures.ThreadPoolExecutor(max_workers=max_threads) as executor:
    #         # 提交任务到线程池
    #         futures = [executor.submit(kg.process_request, i) for i in id_array]

    #         # 等待所有任务执行完毕
    #         concurrent.futures.wait(futures)

    #         # 收集线程的返回结果
    #         results = [future.result() for future in futures if future.result() is not None]

    #     results.sort(key=lambda x: x['singerid'])

    #     filename = f"tmp.json"
    #     file_path = os.path.join(current_dir, filename)
        # with open(file_path, "w", encoding="utf-8") as file:
        #     json.dump(results, file, ensure_ascii=False, indent=4)

    # 5.筛选
    # current_dir = sys.path[0]
    # print(current_dir)
    # filename = f"singerListFull.json"
    # file_path = os.path.join(current_dir, filename)
    # with open(file_path, "r", encoding="utf-8") as file:
    #     content = file.read()
    #     list = json.loads(content)
        
    #     print(f"共有歌手 {len(list)} 位")
    #     songcount = 0
    #     for item in list:
    #         count = item['songcount']
    #         name = item['singername']
    #         songcount+=count
    #     print(f"共有歌曲 {songcount} 首")
    
    # gradeLevel1Singers = [x for x in list if 1 == x['grade']]
    # print(f"热度级别为1的歌手: {len(gradeLevel1Singers)}位")
    # match = []
    # for item in gradeLevel1Singers:
    #     name =  item['singername']
    #     id = item['singerid']
    #     match.append({"name": name, "id": id})
    # print(match)

    # 6.获取top200的歌手的热度前十的歌曲合集，写入top200SingerSongs.json
    # current_dir = sys.path[0]
    # filename = f"hot200singer.json"
    # file_path = os.path.join(current_dir, filename)
    # with open(file_path, "r", encoding="utf-8") as file:
    #     content = file.read()
    #     singerlist = json.loads(content)

    #     allSongs = []
    #     for singer in singerlist:
    #         singerid = singer['id']
    #         songs = kg.getSingerSongs(singerid)
    #         allSongs.extend(songs)
        
    #     songs_file_path = os.path.join(current_dir, 'top200SingerSongs.json')
    #     with open(songs_file_path, "w", encoding="utf-8") as file:
    #         json.dump(allSongs, file, ensure_ascii=False, indent=4)

    # 7.获取歌曲播放信息
    # current_dir = sys.path[0]
    # filename = f"top200SingerSongs.json"
    # file_path = os.path.join(current_dir, filename)
    # with open(file_path, "r", encoding="utf-8") as file:
    #     content = file.read()
    #     songlist = json.loads(content)
    #     hashs = [item['hash'] for item in songlist if 'hash' in item]
    #     ids = []
    #     for hash in hashs:
    #         encodeid = kg.getSong_encode_album_audio_id(hash)
    #         ids.append(encodeid)
    #     print(ids)
    
    # 8.获取歌单信息并下载
    # url = 'https://www.kugou.com/songlist/gcid_3ztqe7r9zcz091/?src_cid=3ztqe7r9zcz091&uid=1562760404&chl=wechat&iszlist=1'
    # ids = kg.getPlayListIds(url)
    # jsons = []
    # for id in ids:
    #     info = kg.getSonginfo(id, token='fbc58a851abbd5cc37efc6aaafa14d88618f9f52e714bf02b31e0e86374528c9')
    #     if not info is None:
    #         jsons.append(info)
    
    # ids = [item['encode_album_audio_id'] for item in jsons if 'encode_album_audio_id' in item]
    # print(ids)

    # file_path = sys.path[0] + '/催眠歌单.json'
    # # 1.创建歌单json文件
    # kg.write(file_path, jsons)
    # # 2.根据文件内容下载歌单歌曲
    # kg.download(file_path)

    # 9.仅下载歌曲
    # token = ''
    # kg.downloadOnly('j7c7xfb', token)

    # id = kg.search("周杰伦")
    # kg.getSonginfo(id)

    # 10.下载歌手的全部歌曲
    # songs = kg.getSingerSongs(3520, pagesize=10)
    # hashs = [item['hash'] for item in songs if 'hash' in item]
    # encodeIds = []
    # result = []
    # for hash in hashs:
    #     encodeIds.append(kg.getSong_encode_album_audio_id(hash))
    # for id in encodeIds:
    #     result.append(kg.getSonginfo(id))
    
    # file_path = sys.path[0] + '/歌单.json'
    # # 1.生成歌单
    # kg.write(file_path, result)
    # # 2.下载歌单的歌曲
    # kg.download(file_path)

    # 11.下载
    # encodeIds = ['mhlly48', 'nw5e476', 'irn9jef', '42zj5t4b', 'sbsgq78', 'gssam85', 'j4nno90', '138ejv1e', 'n9fu5bd', '1dhgijb1', 'gh0mhca', 'imzu9f7', 'g9y5n77', 'j6kfy7b', 'w86quc4', 'p0ood1a', 'o45cf3d', 'grordff', 't6n2ob5', 'zbideee', 'n5k622f', '126zxz84', '30eunm5c', 'nyj0q4c', '2o625r98', 'nu6wc22']
    # result = []
    # for id in encodeIds:
    #     result.append(kg.getSonginfo(id, token='fbc58a851abbd5cc37efc6aaafa14d88618f9f52e714bf02b31e0e86374528c9'))
    
    # file_path = sys.path[0] + '/我的英文歌单.json'
    # # 1.生成歌单
    # kg.write(file_path, result)
    # # 2.下载歌单的歌曲
    # kg.download(file_path)

    #
    # kg.getSingerSongs(3534)
    # kg.getSong_encode_album_audio_id('4FDCFD6F84B9BF0FEACF25EF5C4C1567', '538817761')
