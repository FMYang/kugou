import re
from bs4 import BeautifulSoup as bs
import requests
import os
import json
import sys
import time
import glob
import hashlib

class Base():
    headers = {
                # 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
                'cookie': 'KuGoo=KugooID=1562760404&KugooPwd=62BE7A279A4D2041E62AC32C561FB6A3&NickName=%u660e&Pic=http://imge.kugou.com/kugouicon/165/20191007/20191007151053437428.jpg&RegState=1&RegFrom=&t=fbc58a851abbd5cc37efc6aaafa14d88073d1065fc62d30128e12ef22ec0d7e2&a_id=1014&ct=1700734703&UserName=%u006b%u0067%u006f%u0070%u0065%u006e%u0031%u0035%u0036%u0032%u0037%u0036%u0030%u0034%u0030%u0034&t1=',
                'cookie': 'KugooID=1562760404',
                'cookie': 'UserName=kgopen1562760404',
                'cookie': 'kg_dfid_collect=d41d8cd98f00b204e9800998ecf8427e',
                'cookie': 'kg_mid=c3c3374152df945246969fee3f15f108',
                'cookie': 'kg_dfid=1Jcfej3YdDZy2atfXD11pFRw',
                'cookie': 'Hm_lvt_aedee6983d4cfc62f509129360d6bb3d=1700105234',
                'cookie': 'KuGooRandom=66551700105260332',
                'cookie': 'ACK_SERVER_10015=%7B%22list%22%3A%5B%5B%22bjlogin-user.kugou.com%22%5D%5D%7D',
                'cookie': 't=fbc58a851abbd5cc37efc6aaafa14d88073d1065fc62d30128e12ef22ec0d7e2',
                'cookie': 'mid=c3c3374152df945246969fee3f15f108',
                'cookie': 'dfid=1Jcfej3YdDZy2atfXD11pFRw',
                'cookie': 'Hm_lpvt_aedee6983d4cfc62f509129360d6bb3d=1700734706 if-modified-since: Thu, 16 Nov 2023 09:01:46 GMT',
                'CheckCode': 'czozMjoiNGZkZmJlYWQ1YzQwY2MwNTc3YjU5NjVlNzQ0NzVlNDAiOw%3D%3D'
            }

    def __init__(self):
        pass

    def make_dir(self, dir: str):
        if os.path.exists(dir):
            pass
        else:
            print(f'Making the directory {dir}')
            os.mkdir(dir)

    def get_text(self, url: str):
        url = self.url
        text = requests.get(url, headers=self.headers).text

        return text
    
class Song(Base):
    # the regex
    hash_regex = re.compile('"hash":"([0-9A-Za-z]*)",')
    album_id_regex = re.compile('"album_id":([0-9]*),')
    song_link_regex = re.compile('https://www.kugou.com/mixsong/[0-9A-Za-z]*.html')
    data_regex = re.compile(r'jQuery.*?[({].*?"data":(.*?)}[)]', re.S)
    encode_album_audio_id_pattern = r'/([^/]+)\.html'
    jsons = []

    def __init__(self, url):
        self.url = url
        self.jsons = []
        self.encode_album_audio_id = re.search(self.encode_album_audio_id_pattern, url).group(1)
        text = self.get_text(url)
        self.get_info(text)

    def get_info(self, text):
        song_soup = bs(text, 'html.parser')
        script = str(song_soup.script.string)
        try:
            hash = self.hash_regex.search(script).group(1)
            album_id = self.album_id_regex.search(script).group(1)

            # vip歌曲地址（需要vip信息签名，token和userId）
            template = "https://wwwapi.kugou.com/play/songinfo?srcappid=2919&clientver=20000&clienttime=%s&mid=%s&uuid=%s&dfid=%s&appid=1014&platid=4&encode_album_audio_id=%s&token=fbc58a851abbd5cc37efc6aaafa14d88e3e9cb69826e8e8fad51debe6516da13&userid=1562760404&signature=%s"
            ts = str(int(time.time() * 1000))
            mid = "c3c3374152df945246969fee3f15f108"
            uuid = mid
            dfid = '1Jcfej3YdDZy2atfXD11pFRw'
            sign_template = "NVPh5oo715z5DIWAeQlhMDsWXXQV4hwtappid=1014clienttime=%sclientver=20000dfid=%sencode_album_audio_id=%smid=%splatid=4srcappid=2919token=fbc58a851abbd5cc37efc6aaafa14d88e3e9cb69826e8e8fad51debe6516da13userid=1562760404uuid=%sNVPh5oo715z5DIWAeQlhMDsWXXQV4hwt"
            sign_str = sign_template % (ts, dfid, self.encode_album_audio_id, mid, uuid)
            signature = hashlib.md5(sign_str.encode()).hexdigest()
            songurl = template % (ts, mid, uuid, dfid, self.encode_album_audio_id, signature)
            response = requests.get(songurl)  # don't use cookies here
            response.raise_for_status() # 校验状态码
            resp = response.text
            data = json.loads(resp)['data']

            # 免费歌曲地址
            # data_url = f"https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback=jQuery191053318263960215_1626866592344&hash={hash}&dfid=1tXkst0i97Rq4RW0pz15GjrP&mid=3196606d7d3ff0207a473da58e0b44b3&platid=4&album_id={album_id}&_=1626866592346"
            # resp = requests.get(data_url, headers=self.headers).text
            # resp.raise_for_status() # 校验状态码
            # data_text = self.data_regex.findall(resp)[0]
            # data = json.loads(data_text)

            self.song_name = data.get('song_name', "")
            self.lrc = data.get('lyrics', "")
            self.play_url = data.get('play_url')
            self.play_backup_url = data.get('play_backup_url', "")
            self.timelength = data.get('timelength')
            self.filesize = data.get('filesize')
            self.author_name = data.get('author_name', "")
            self.timelength = data.get('timelength')
            self.is_free_part = data.get('is_free_part')
            self.img = data.get('img', "")
            self.encode_album_id = data.get('encode_album_id', "")
            self.encode_album_audio_id = data.get('encode_album_audio_id', "")
            self.album_audio_id = data.get('album_audio_id', "")
            self.album_id = data.get('album_id', "")
            self.author_id = data.get('author_id', "")
            self.video_id = data.get('video_id', "")
            print(f"{self.song_name} is_free_part={self.is_free_part}")
            if self.is_free_part is None:
                print(f"{self.encode_album_audio_id}失败 去校验验证码，然后继续")
                time.sleep(7)
        except:
            print("except")
            pass

    def run_song(self, url):
        text = requests.get(url, headers=self.headers).text
        regex = re.compile('https://www.kugou.com/mixsong/[a-zA-Z0-9]*?\.html')
        url = regex.match(url).group()
        try:
            song = Song(url)
            if not song.play_url is None \
            and not song.song_name is None \
            and song.is_free_part == 0:
                data = {
                    "song_name": song.song_name,
                    "play_url": song.play_url,
                    "play_backup_url": song.play_backup_url, 
                    "timelength": song.timelength,
                    "filesize": song.filesize,
                    "author_name": song.author_name, 
                    "timelength": song.timelength, 
                    "is_free_part": song.is_free_part, 
                    "img": song.img,
                    "encode_album_id": song.encode_album_id, 
                    "encode_album_audio_id": song.encode_album_audio_id,
                    "album_audio_id": song.album_audio_id, 
                    "album_id": song.album_id,
                    "author_id": song.author_id, 
                    "video_id": song.video_id,
                    "lrc": song.lrc
                    }
                self.jsons.append(data)
        except:
            pass

    def get_songlinks(id):
        urls = ['https://www.kugou.com/yy/rank/home/{}-{}.html'.format(str(i), id)
                for i in range(1, 24)]
        urlList = []
        for url in urls:
            try:
                response = requests.get(url, headers=Base.headers)
                response.raise_for_status() # 校验状态码
                song_soup = bs(response.text, 'html.parser')
                song_link_regex = re.compile('https://www.kugou.com/mixsong/[0-9A-Za-z]*\.html')
                links = song_soup.find_all('a', {'href': song_link_regex})
                for link in links:
                    link = link['href']
                    urlList.append(link)
            except:
                pass

        return urlList
    
    def get_songList(url):
        urlList = []
        try:
            response = requests.get(url, headers=Base.headers)
            response.raise_for_status() # 校验状态码
            song_soup = bs(response.text, 'html.parser')
            song_link_regex = re.compile('https://www.kugou.com/mixsong/[0-9A-Za-z]*\.html')
            links = song_soup.find_all('a', {'href': song_link_regex})
            listName = song_soup.find_all('div', class_='mbx')[0].find('span').text
            for link in links:
                link = link['href']
                urlList.append(link)
        except:
            pass

        return (listName, urlList)

# 增量合并到原榜单
def merge_data():
    # 获取当前目录
    current_dir = sys.path[0]

    print(current_dir)
    # 使用 glob 模块匹配当前目录下的所有 json 文件
    json_files = glob.glob(os.path.join(current_dir, "**/*.json"), recursive=True)

    jsons = []
    free_songs = []
    for tmpFile in json_files:
        fileName = os.path.basename(tmpFile)
        if "free_songs" in fileName:
            json_files.remove(tmpFile)

    try:
        # 打印找到的所有 json 文件路径，将所有内容放入jsons数组中
        for file_path in json_files:
            print(file_path)

            fileName = os.path.basename(file_path)
            destinationPath = f"/Users/yfm/ZY/github/kugou/raw/{fileName}"

            file_free_songs = []
            if os.path.exists(file_path):
                # 新抓的文件
                with open(file_path, "r", encoding="utf-8") as file:
                    content = file.read()
                    try:
                        data = json.loads(content)   
                        jsons.extend(data)                 
                        # for item in data:
                        #     if not item["is_free_part"]:
                        #         file_free_songs.append(item)
                        # print(f"{file_path}的免费歌曲有{len(file_free_songs)}首")

                        # # 原文件
                        # with open(destinationPath, "r", encoding="utf-8") as file:
                        #     sourceContent = file.read()
                        # try:
                        #     sourceData = json.loads(sourceContent) 
                        #     for freesong in file_free_songs:
                        #         exist = False
                        #         for sourceSong in sourceData:
                        #             if freesong["album_audio_id"] == sourceSong["album_audio_id"]:
                        #                 exist = True
                        #                 break

                        #         if not exist:
                        #             newsongName = freesong["song_name"]
                        #             newid = freesong["album_audio_id"]
                        #             print(f"{fileName} 新增免费歌曲 {newsongName} {newid}")
                        #             sourceData.insert(0, freesong)
                            
                        #     # 保存

                        #     with open(destinationPath, "w", encoding="utf-8") as file:
                        #         json.dump(sourceData, file, ensure_ascii=False, indent=4)
                            
                        #     jsons.extend(sourceData)

                        # except json.JSONDecodeError as e:
                        #     print("loads except", e)
                        #     pass

                    except json.JSONDecodeError as e:
                        print("loads except", e)
                        pass


        # 过滤vip歌曲和没有播放地址的，将免费的歌曲内容放入free_songs数组中
        for song in jsons:
            is_free_part = song.get('is_free_part', "")
            play_url = song.get('play_url', "")
            if not is_free_part and len(play_url) > 0:
                free_songs.append(song)

        # 过滤重复的歌曲
        unique_ids = {}
        for item in free_songs:
            try:
                item_id = item["album_audio_id"]
                unique_ids[item_id] = item
            except:
                pass

        # 去重后的免费歌曲信息列表
        uniqueList = list(unique_ids.values())

        print(f"全部榜单总共有{len(jsons)}首歌曲")
        # print(f"免费歌曲有{len(free_songs)}首")
        print(f"其中免费不重复的歌曲有{len(uniqueList)}首")

        savePath = os.path.join(current_dir, "free_songs.json")
        with open(savePath, "w", encoding="utf-8") as file:
            json.dump(uniqueList, file, ensure_ascii=False, indent=4)
    except:
        print("merge except")
        pass

# 下载新增的歌曲
def download():
    current_dir = sys.path[0]
    musicDir = current_dir + "/music"
    json_path = current_dir + "/热门歌手代表作.json"

    with open(json_path, 'r') as file:
        data = file.read()
        content = json.loads(data)

        for song in content:
            song_name = song['song_name']

            songFile = musicDir + f"/{song_name}.mp3"

            try:
                if not os.path.exists(songFile):
                    play_url = song['play_url']

                    # print(f"{songFile}   文件不存在")
                    print(f'Downloading {song_name}')

                    try:
                        headers = {
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:107.0) Gecko/20100101 Firefox/107.0',
                        }

                        content = requests.get(play_url, headers=headers).content
                    except:
                        content = bytes()

                    try:
                        savepath = os.path.join(musicDir, f'{song_name}.mp3')
                        with open(savepath, 'wb') as f:
                            f.write(content)
                    except:
                        pass
                else:
                    # print(f"{songFile} 文件存在")
                    pass
            except:
                pass

def getSongInfo():
    songList = [
                # {"title": "酷狗飙升榜", "id": 6666},
                # {"title": "top500", "id": 8888},
                # {"title": "80后热歌榜", "id": 49225},
                # {"title": "90后热歌榜", "id": 49223}, 
                # {"title": "00后热歌榜", "id": 49224},
                # {"title": "蜂鸟流行音乐榜", "id": 59703},
                # {"title": "抖音热歌榜", "id": 52144},
                # {"title": "快手热歌榜", "id": 52767},
                # {"title": "DJ热歌榜", "id": 24971},
                # {"title": "内地榜", "id": 31308},
                # {"title": "香港地区榜", "id": 31313},
                # {"title": "台湾地区榜", "id": 54848},
                # {"title": "欧美榜", "id": 31310},
                # {"title": "综艺新歌榜", "id": 46910},
                # {"title": "说唱先锋榜", "id": 44412},
                # {"title": "影视金曲榜", "id": 33163},
                # {"title": "粤语金曲榜", "id": 33165},
                # {"title": "ACG新歌榜", "id": 33162},
                # {"title": "酷狗音乐人原创榜", "id": 30972},
                # {"title": "酷狗雷达榜", "id": 37361},
                # {"title": "joox本地热歌榜", "id": 42807},
                # {"title": "KKBOX风云榜", "id": 42808},
                # {"title": "电音热歌榜", "id": 33160},
                # {"title": "韩国榜", "id": 31311},
                # {"title": "日本榜", "id": 31312}, 
                # {"title": "欧美金曲榜", "id": 33166},
                # {"title": "美国BillBoard榜", "id": 4681},
                # {"title": "英国单曲榜", "id": 4680},
                # {"title": "日本公信榜", "id": 4673},
                # {"title": "韩国Melon音乐榜", "id": 38623},
                # {"title": "日本SPACE SHOWER榜", "id": 46868},
                # {"title": "Beatport电子舞曲榜", "id": 25028}, 
                # {"title": "小语种热歌榜", "id": 36107}
                ]
    
    # 下载酷狗榜单歌
    # for l in songList:
    #     Song.jsons.clear()
    #     title = l['title']
    #     channelId = l['id']
    #     urls = Song.get_songlinks(channelId)
    #     for url in urls:
    #         Song.run_song(Song, url)
    #         # time.sleep(1) # 防止反爬虫验证码
    #     jsons = Song.jsons

    #     current_dir = sys.path[0]
    #     filename = f"{title}.json"
    #     file_path = os.path.join(current_dir, filename)
    #     with open(file_path, "w", encoding="utf-8") as file:
    #         json.dump(jsons, file, ensure_ascii=False, indent=4)
    #     time.sleep(1)

    # 下载自己的歌单
    tmpList = []

    Song.jsons.clear()
    urls = []
    for id in tmpList:
        urls.append(f"https://www.kugou.com/mixsong/{id}.html")
    
    for url in urls:
        Song.run_song(Song, url)
    jsons = Song.jsons

    current_dir = sys.path[0]
    filename = "热门歌手代表作.json"
    file_path = os.path.join(current_dir, filename)
    with open(file_path, "w", encoding="utf-8") as file:
        json.dump(jsons, file, ensure_ascii=False, indent=4)
    time.sleep(1)

# 下载歌单
def getSongListInfo():
    Song.jsons.clear()
    urls = []
    # 歌单URL
    link = 'https://www.kugou.com/songlist/gcid_3ztqe7r9zbz0ab/?src_cid=3ztqe7r9zbz0ab&uid=1562760404&chl=wechat&iszlist=1'
    name, urls = Song.get_songList(link)
    for url in urls:
        Song.run_song(Song, url)
    jsons = Song.jsons

    current_dir = sys.path[0]
    filename = f"{name}.json"
    file_path = os.path.join(current_dir, filename)
    with open(file_path, "w", encoding="utf-8") as file:
        json.dump(jsons, file, ensure_ascii=False, indent=4)
    time.sleep(1)

if __name__ == '__main__':   

    # 获取歌单信息
    # getSongListInfo() 

    # 1.获取歌曲json信息
    # getSongInfo()
    
    # 2.通过歌曲信息，整合数据到free_songs.json文件
    merge_data()

    # 3.根据整合的数据，下载未下载的歌曲
    # download()